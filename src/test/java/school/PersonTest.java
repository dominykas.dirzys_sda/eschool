package school;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PersonTest {
    // act given when then
    @Test
    void getAge_When_Person_Born_Today_Returns_0(){
        // Arrange
        Person person = new Person("a", "a", LocalDate.now(), 0, 0, Gender.MALE);

        // Act
        int age = person.getAge();

        assertEquals(0, age);
    }

    @Test
    void setName_When_Name_Is_Empty_String_Throws_Exception(){
        Person person = new Person("", "a", LocalDate.now(), 0, 0, Gender.MALE);

        // ismes IllegalArgumentException
    }
}
