package school;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
